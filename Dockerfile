###############################################################################

FROM ruby:3-alpine

###############################################################################

ARG HTML_PROOFER_VERSION="5.0.9"

###############################################################################

RUN apk add --no-cache                                                              \
      libcurl                                                                    && \
    apk add --no-cache --virtual "build-deps"                                       \
      build-base                                                                 && \
    gem install html-proofer --version "= ${HTML_PROOFER_VERSION}" --no-document && \
    apk del "build-deps"

###############################################################################

ENTRYPOINT [ "/usr/local/bundle/bin/htmlproofer" ]

###############################################################################
